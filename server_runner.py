import os
import json
import time

from websocket_server import WebSocketServer
from config import PORT, SEND_INTERVAL, DEFAULT_PATH, SERIALIZER_JUMPS


def get_serializers(json_dir):
    files = os.listdir(json_dir)
    serializers = set(f.split('_')[0] for f in files)
    indexes_by_serializer = {s: sorted(int(os.path.splitext(f)[0].split('_')[1]) for f in files if f.split('_')[0] == s)
                             for s in serializers}
    return list(serializers)


def create_reader_for_serializer(serializer, json_dir, ext='json'):
    def reader(counter):
        filename = os.path.join(json_dir, '{}_{}.{}'.format(serializer, counter, ext))
        if not os.path.exists(filename):
            return None
        with open(filename) as f:
            return json.load(f)
    return reader


def read_messages(counter, readers, messages=None):
    if messages is None:
        messages = {}

    for reader_type, reader_func in readers:
        if reader_type in messages:
            continue  # already have a message
        message = reader_func(counter)
        messages[reader_type] = message

    return messages


def send_all_messages(server, readers, interval, messages_intervals=None, start=0):
    if messages_intervals is None:
        messages_intervals = {}
    for reader_type in readers.keys():
        messages_intervals.setdefault(reader_type, 1)

    counter = start
    while len(readers) > 0:
        messages_to_send = {}
        for serializer_name, reader_func in readers.items():
            if counter % messages_intervals[serializer_name] == 0:
                read_messages(counter / messages_intervals[serializer_name], [(serializer_name, reader_func)], messages_to_send)

        while server.get_num_of_clients() == 0:
            time.sleep(interval)

        for reader_type, message in messages_to_send.items():
            if message is None:
                del readers[reader_type]
                print("no more messages from this reader {} counter {}".format(reader_type, counter))
            else:
                server.send(message)
            del messages_to_send[reader_type]

        time.sleep(interval)
        counter += 1


def main():
    serializers = get_serializers(DEFAULT_PATH)
    print("serializers: " + ", ".join(serializers))
    readers = {serializer: create_reader_for_serializer(serializer, DEFAULT_PATH) for serializer in serializers}

    try:
        server = WebSocketServer(PORT)
        server.start_listen()
        server.wait_for_connection()
        send_all_messages(server, readers, SEND_INTERVAL, SERIALIZER_JUMPS)
    except ValueError as e:
        print(e)
    except KeyboardInterrupt:
        print("websocket server is closed")


if __name__ == '__main__':
    main()
