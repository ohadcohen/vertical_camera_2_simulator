import json
import socket
import time
from functools import partial
from collections import defaultdict

from threading import Thread, Event
from websocket import create_connection

from viewer import Viewer
from config import SERVER, PORT, COLORS


class WebSocketClient:
    def __init__(self, host, websocket_port, colors):
        self.websocket_server = 'ws://{host}:{port}'.format(host=host, port=websocket_port)
        self._websocket = None
        self._receive_event = Event()
        self._receiving_thread = Thread(target=self._receive_loop)
        self._viewer = defaultdict(partial(Viewer, colors))

    @staticmethod
    def _get_platform_id(global_id):
        img_magic, mission_id, platform_id, date, frame_id = global_id.split('_')
        return platform_id

    def _receive_loop(self):
        # start_time = time.time()
        while self._receive_event.is_set():
            recv = self._websocket.recv()
            if recv == '':  # socket was closed
                continue
            parsed = json.loads(recv)
            key = parsed.keys()[0]

            # arrive_time = "{:08.2f}".format(time.time() - start_time)
            # print("client got {} at {}".format(key, arrive_time))

            if key == 'Image':
                img_data = parsed['Image']['ImageBuf']['RawData'].decode('base64')
                platform_id = self._get_platform_id(parsed['Image']['GlobalId'])
                self._viewer[platform_id].update_image(img_data, parsed['Image']['ThinningFactor'])
                self._viewer[platform_id].show(platform_id)

            elif key == 'Tracks':
                # print("tracks received: {}".format(len(parsed['Tracks'])))
                if len(parsed['Tracks']) > 0:
                    tracks_by_platform = defaultdict(list)
                    for track in parsed['Tracks']:
                        platform_id = self._get_platform_id(track['Detection'][0]['ImageID'])
                        tracks_by_platform[platform_id].append(track)
                    for platform, tracks in tracks_by_platform.items():
                        self._viewer[platform].update_tracks(tracks)
                        self._viewer[platform].show(platform)

    def connect(self):
        try:
            self._websocket = create_connection(self.websocket_server)
            print("Client connected to {}".format(self.websocket_server))
            self._receive_event.set()
            self._receiving_thread.start()
            return True
        except socket.error as e:
            if e.errno == 111:  # Connection refused
                print(e)
                return False
            else:
                raise

    def stop(self):
        print("Client stopping")
        self._receive_event.clear()
        if self._receiving_thread.is_alive():
            self._receiving_thread.join()
        if self._websocket is not None:
            self._websocket.close()

    def wait_for_stop(self):
        self._receive_event.wait()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._receive_event.is_set():
            self.stop()


def main():
    client = WebSocketClient(SERVER, PORT, COLORS)
    try:
        client.connect()
        raw_input("Press any key to Exit...\n")
    except KeyboardInterrupt:
        print("Client stopped by the user")
    finally:
        client.stop()


if __name__ == '__main__':
    main()
