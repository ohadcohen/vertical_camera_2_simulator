
# Messages Config
DEFAULT_PATH = "websocket_messages"
START_INDEX = 0
END_INDEX = 999

IMAGE_SERIALIZER = "VerticalCameraImageSerializer"
TRACKS_SERIALIZER = "VerticalCameraTracksSerializer"
SERIALIZERS = [IMAGE_SERIALIZER, TRACKS_SERIALIZER]


# Websocket Server Config
PORT = 12345
SERVER = 'localhost'

SEND_TRACKS_EVERY = 1
SEND_INTERVAL = .33
SERIALIZER_JUMPS = {IMAGE_SERIALIZER: 15,
                    TRACKS_SERIALIZER: 1}


COLORS = {'background': 'black',
          'bus': 'white',
          'car': 'green',
          'commercial': 'blue',
          'motorcycle': 'purple',
          'person': 'pink',
          'tractor': 'cyan',
          'truck': 'yellow',
          'vmd': 'red',
          'pick-up': 'red',
          'van': 'red',
          'cement mixer': 'red'}
