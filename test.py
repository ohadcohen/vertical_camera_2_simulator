import os
import time
from threading import Thread, Event

from websocket_server import WebSocketServer
from websocket_client import WebSocketClient
from config import DEFAULT_PATH, START_INDEX, END_INDEX, SEND_INTERVAL, PORT, SERIALIZER_JUMPS, SERVER, COLORS


def send_loop(server, run_event):
    print("Sending messages...")
    for i in range(START_INDEX, END_INDEX + 1):
        for s, every in SERIALIZER_JUMPS.items():
            if i % every == 0:
                f = os.path.join(DEFAULT_PATH, "{}_{}.json".format(s, i / every))
                # print("sending {}".format(f))
                with open(f) as fp:
                    data = fp.read()
                    server.send(data)
        time.sleep(SEND_INTERVAL)
        if not run_event.is_set():
            break


def main():
    server = WebSocketServer(port=PORT)
    client = WebSocketClient(SERVER, PORT, COLORS)
    run_event = Event()
    sending_loop_thread = Thread(target=send_loop, args=(server,run_event))

    try:
        print("Starting...")
        server.start_listen()
        while not client.connect():
            time.sleep(SEND_INTERVAL)

        run_event.set()
        sending_loop_thread.start()
        raw_input("Press any key to Exit...\n")

    except KeyboardInterrupt:
        print("Got keyboard interrupt. Exiting...")

    finally:
        client.stop()
        run_event.clear()
        if sending_loop_thread.is_alive():
            sending_loop_thread.join()
        server.stop()
        print("Stopped")


if __name__ == '__main__':
    main()
