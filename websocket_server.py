import time
import socket
from threading import Thread, Event

import tornado.ioloop

from websocket_wrapper import WebSocket


class WebSocketServer(object):
    def __init__(self, port):
        super(WebSocketServer, self).__init__()
        self._port = port
        self._websocket = WebSocket
        self._listening_thread = None
        self._listening = Event()

    def _listen_thread(self):
        try:
            application = tornado.web.Application([(r"/", self._websocket), ])
            application.listen(self._port)
            print("Server start listening on port {}".format(self._port))
            self._listening.set()
            tornado.ioloop.IOLoop.instance().start()
        except socket.error as e:
            if e.errno == 98:  # Address already in use
                print(e)
            else:
                raise

    def start_listen(self):
        self._listening_thread = Thread(target=self._listen_thread)
        self._listening_thread.daemon = True  # this thread should operate only when there other threads
        self._listening_thread.start()
        if not self._listening.wait(1):
            raise ValueError("Server can't listen on port {}".format(self._port))

    def send(self, data):
        # print("sending to {} clients".format(self.get_num_of_clients()))
        for client in self._websocket.clients:
            try:
                client.write_message(data, binary=True)
            except Exception as e:
                # AttributeError: 'NoneType' object has no attribute 'advance'
                # BufferError: Existing exports of data: object cannot be re-sized
                # WebSocketClosedError
                print("Exception while sending:\n{}".format(e))

    def stop(self):
        print("Server stopping")
        tornado.ioloop.IOLoop.instance().stop()
        self._listening_thread.join()

    def get_num_of_clients(self):
        return len(self._websocket.clients)

    def wait_for_connection(self):
        if self.get_num_of_clients() > 0:
            return

        print("waiting for connections")
        while self.get_num_of_clients() == 0:
            time.sleep(1)
        print("got new connection")
