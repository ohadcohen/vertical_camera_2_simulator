import matplotlib

import numpy as np

import cv2


class Viewer(object):
    def __init__(self, classes_to_colors):
        self._image = None
        self._tracks = None
        self._classes_to_colors = classes_to_colors
        self._thickness = 2

    @staticmethod
    def _color_hex_to_tuple(c):
        return int(c[1:3].lower(), 16), int(c[3:5].lower(), 16), int(c[5:7].lower(), 16)

    def _draw_boxes_to_image(self):
        """Draw boxes in image and write class and sub class"""
        font = cv2.FONT_HERSHEY_SIMPLEX
        image = np.array(self._image)
        if self._tracks is not None:
            for crop in self._tracks:
                crop_cls = crop['Class'].lower()
                color = self._classes_to_colors[crop_cls]
                crop['Detection'][-1]['TargetSize']
                x1 = int(crop['Detection'][-1]['TargetSize']['URBoundingBox']['i']) / self._thinning_factor
                y1 = int(crop['Detection'][-1]['TargetSize']['URBoundingBox']['j']) / self._thinning_factor
                x2 = int(crop['Detection'][-1]['TargetSize']['LLBoundingBox']['i']) / self._thinning_factor
                y2 = int(crop['Detection'][-1]['TargetSize']['LLBoundingBox']['j']) / self._thinning_factor
                cv2.rectangle(image, (int(x1), int(y1)), (int(x2), int(y2)), self._color_hex_to_tuple(matplotlib.colors.get_named_colors_mapping()[color]), thickness=self._thickness)

                title = crop['TrackId']
                cv2.rectangle(image, (int(x1), int(y1) - 10), (int(x1) + len(title) * 7, int(y1)), (0, 0, 0), -1)
                cv2.putText(image, title, (int(x1) - 1, int(y1)), font, 0.4, (255, 255, 255), 1)

        return image

    def update_image(self, bin, thinning_factor):
        self._image = cv2.imdecode(np.frombuffer(bin, np.uint8), cv2.IMREAD_COLOR)
        self._thinning_factor = thinning_factor

    def update_tracks(self, tracks):
        self._tracks = tracks

    def show(self, title=''):
        if self._image is not None:
            image = self._draw_boxes_to_image()
            cv2.imshow(title, cv2.resize(image, (0, 0), fx=.5, fy=.5))
            cv2.waitKey(1)
