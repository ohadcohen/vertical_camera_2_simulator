from tornado import websocket


class WebSocket(websocket.WebSocketHandler):
    """
    Manage the web socket connection and manage list of clients
    """
    clients = []

    def __init__(self, *args, **kwargs):
        super(WebSocket, self).__init__(*args, **kwargs)

    def on_message(self, message):
        pass

    def on_close(self):
        # print("client disconnected")
        WebSocket.clients.remove(self)

    def check_origin(self, origin):
        return True

    def open(self):
        # print("client connected")
        WebSocket.clients.append(self)

    def data_received(self, chunk):
        pass
